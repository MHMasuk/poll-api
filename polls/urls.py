from django.urls import path

from rest_framework.routers import DefaultRouter
from polls import views

router = DefaultRouter()
router.register('polls', views.PollViewSet, base_name='polls')

app_name = 'polls'

urlpatterns = [
    # path('polls/', views.PollList.as_view(), name='polls_list'),
    # path('polls/<int:pk>', views.PollDetail.as_view(), name='polls_detail'),
    path("polls/<int:pk>/choices/", views.ChoiceList.as_view(), name="choice_list"),
    path("polls/<int:pk>/choices/<int:choice_pk>/vote/", views.CreateVote.as_view(), name="create_vote"),
]


urlpatterns += router.urls
