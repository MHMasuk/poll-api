from django.urls import path

from users import views

app_name = 'users'

urlpatterns = [
    path('users/', views.UserCreate.as_view(), name='user_create'),
    path('login/', views.LoginView.as_view(), name='login'),
]